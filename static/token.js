"use strict";

/*global $:false, console:false */

function log(message) {
	if (console && console.log) {
		console.log(message);
	}
}

var security = {

	add_token_to_form : function(e) {
		var $form = $(e.target);

		var token = security.getTokenFromCookieOrUrl();

		log(token);

		$form.prepend('<input type="hidden" name="token" value="' + token + '" />');
		//e.preventDefault();
	},
	getTokenFromCookieOrUrl : function () {
		var token = security.getTokenFromCookie();
		if (!token) {
			token = security.getTokenFromUrl();
		}
		return token;
	},
	getTokenFromCookie : function() {
		return $.cookie('token');
	},
	getTokenFromUrl : function () {
		$.ajax({
			url: '/set-token-cookie',
			method: 'POST',
			async: false
		});
		return security.getTokenFromCookie();
	}
};

var prototypeHelpers = {
	replaceWithCookieValue : function (element) {
		$(element).val($.cookie($(element).data('cookie-name')));
	},
	saveCookies : function (event) {
		var $form = $(event.target);
		$form.find('.save-cookie').each(function (index, value) {
			var $input = $(value);
			var cookieName = $input.data('cookie-name');
			if (cookieName) {
				$.cookie(cookieName, $input.val());
			}
		});
	}
};


$(document).ready(function() {

	$('body').on('submit', 'form.add-token-on-submit', security.add_token_to_form);

	$('.replace-with-cookie-value').each(function (index, value) {
		prototypeHelpers.replaceWithCookieValue(value);
	});

	$('body').on('submit', 'form.save-cookies', prototypeHelpers.saveCookies);
});