import base64
from functools import wraps
import uuid
from flask import Flask, render_template, redirect, url_for, session, request, make_response
import os

app = Flask(__name__)

app.debug = True
app.secret_key = '6959b8bfb6ed4fe7bf8ac1ec211a593a'


def token_interceptor(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):

        # Here we have 2 options for the js_error_fallback:
        # 1. get the token from the cookie
        # 2. just bypass the token check
        if not 'js_error_fallback_active' in session and not is_token_valid():
            response = redirect(url_for('token_error_fallback'))
        else:
            response = f(*args, **kwargs)

        rotate_token(response)

        return response
    return decorated_function


def is_token_valid():
    if not 'token' in request.form or not 'token' in session:
        return False
    else:
        return request.form['token'] == session['token']


# This URL should be an HTTPS URL in the real application so that it is not cached
# We could also use the regular token, but here we will use the same token for the sake of simplicity
@app.route('/token-error-fallback', methods=['GET'])
def token_error_fallback():
    return render_template('token_error_fallback.html')


@app.route('/token-error-fallback/submit', methods=['POST'])
@token_interceptor
def token_error_fallback_submit():
    session['js_error_fallback_active'] = True

    return redirect(url_for('show_comments'))


@app.route('/set-token-cookie', methods=['POST'])
def set_token_cookie():

    response = make_response(redirect(url_for('show_comments')))
    rotate_token(response)
    return response


def rotate_token(response):
    token = generate_token()
    session['token'] = token
    response.set_cookie('token', token)


def generate_token():
    uid = uuid.uuid4()
    return uid.hex


@app.route('/', methods=['GET'])
def show_comments():
    comments = []
    if 'comments' in session:
        comments = session['comments']

    return render_template('show_comments.html', comments=comments)


@app.route('/comment/submit', methods=['POST'])
@token_interceptor
def save_comment():
    if not 'comments' in session:
        session['comments'] = []

    comment = request.form['comment']
    session['comments'].append(comment)

    return redirect(url_for('show_comments'))


@app.route('/clear-session', methods=['GET'])
def clear_session():
    session.clear()

    response = redirect(url_for('show_comments'))
    response.delete_cookie('token', '/')

    return response


if __name__ == '__main__':
    app.run(host='0.0.0.0')

